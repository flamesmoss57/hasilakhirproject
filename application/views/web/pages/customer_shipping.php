

<div class="main">
    <div class="content" style="text-align: center">
        <div class="register_account" style="text-align:center;display:inline-block;float: none">
            <h3>Alamat yang Dituju</h3>
            <style type="text/css">
                #result{color:red;padding: 5px}
                #result p{color:red}
            </style>
            <div id="result">
                <p><?php echo $this->session->flashdata('message'); ?></p>
            </div>
            <form method="post" action="<?php echo base_url('customer/save/shipping/address');?>">
                <table>
                    <tbody>
                        <tr>
                            <td>
                                <div>
                                    <input type="text" name="shipping_name" placeholder="Masukan Nama">
                                </div>


                                <div>
                                    <input type="text" name="shipping_city" placeholder="Masukan Kota">
                                </div>
                                <div>
                                    <input type="text" name="shipping_phone" placeholder="Masukan nomor hp">
                                </div>
                                <div>
                                    <input type="text" name="shipping_zipcode" placeholder="Masukan Kode Pos">
                                </div>
                            </td>
                            <td>
                                <div>
                                    <input type="text" name="shipping_email" placeholder="Masukan Email">
                                </div>

                                <div>
                                    <input type="text" name="shipping_address" placeholder="Masukan Alamat">
                                </div>
                                
                                <div>
                                <select id="country" name="shipping_country" class="frm-field required">
                                        <option value="null">Pilih Provisi</option>         
                                        <option value="JTe">Jawa Tengah</option>
                                        <option value="JB">Jawa Barat</option>
                                        <option value="JT">Jawa Timur</option>
                                        <option value="YK">Yogyakarta</option>
                                        <option value="JKT">Jakarta</option>
                                        <option value="BT">Banten</option>
                                        <option value="LP">Lampung</option>
                                        <option value="SS">Sumatera Selatan</option>
                                        <option value="NTB">Nusa Tengara Barat</option>
                                        <option value="NTT">Nusa Tengara Timur</option>
                                        <option value="BK">Bengkulu</option>
                                        <option value="JB">Jambi</option>


                                    </select>
                                </div>		

                                
                            </td>
                        </tr> 
                    </tbody></table> 
                <div class="search"><div><button class="grey">Buat Alamat</button></div></div>
                <p class="terms">By clicking 'Buat Alamat' you agree to the <a href="#">Terms &amp; Conditions</a>.</p>
                <div class="clear"></div>
            </form>
        </div>  	
        <div class="clear"></div>
    </div>
</div>